# Introduction

This container is meant for adding dhcp-support into DockerNAT network so that vagrant can use same virtualswitch without furhter hacking.

## Prerequsities

* Windows 10 Professional
* HyperV
* Docker for Windows

## Setup Natnetworking
Add Natnetwork. By default docker for windows vm with ip in 10.0.75.2 wich is in network 10.0.75.2/24 and GW is 10.0.75.1. If these values are not correct you need to start hacking.

```Powershell:
New-NetNat -Name DockerNAT -InternalIPInterfaceAddressPrefix 10.0.75.0/24
```

## Run DCHP Server in container

```Bash:
docker service create --name dhcpd --network host registry.gitlab.com/comiq/dockernat-dhcpd-container:latest
```
